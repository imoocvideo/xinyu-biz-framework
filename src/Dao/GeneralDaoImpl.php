<?php

namespace Codeages\Biz\Framework\Dao;

use Codeages\Biz\Framework\Context\Biz;

abstract class GeneralDaoImpl implements GeneralDaoInterface
{
    protected $biz;

    protected $table = null;

    protected $timestamps = [];

    protected $serializes = [];

    public function __construct(Biz $biz)
    {
        $this->biz = $biz;
    }

    public function create($fields)
    {
        $timestampField = $this->_getTimestampField('created');
        if ($timestampField) {
            $fields[$timestampField] = time();
        }

        $timestampField = $this->_getTimestampField('updated');
        if ($timestampField) {
            $fields[$timestampField] = time();
        }
        $affected = $this->db()->insert($this->table(), $fields);
        if ($affected <= 0) {
            throw $this->createDaoException('Insert error.');
        }

        // todo 不确定lastInsertId()是否一定获取的是主库的数据
        return $this->getWithHint($this->db()->lastInsertId());
    }

    public function update($id, array $fields)
    {
        $timestampField = $this->_getTimestampField('updated');
        if ($timestampField) {
            $fields[$timestampField] = time();
        }

        $this->db()->update($this->table, $fields, ['id' => $id]);

        return $this->getWithHint($id);
    }

    public function delete($id)
    {
        return $this->db()->delete($this->table(), ['id' => $id]);
    }

    public function wave(array $ids, array $diffs)
    {
        $sets = array_map(function ($name) {
            return "{$name} = {$name} + ?";
        }, array_keys($diffs));

        $marks = str_repeat('?,', count($ids) - 1).'?';

        $sql = "UPDATE {$this->table()} SET ".implode(', ', $sets)." WHERE id IN ($marks)";

        return $this->db()->executeUpdate($sql, array_merge(array_values($diffs), $ids));
    }

    public function get($id, $lock = false)
    {
        $sql = "SELECT * FROM {$this->table()} WHERE id = ?".($lock ? ' FOR UPDATE' : '');

        return $this->db()->fetchAssoc($sql, [$id]) ?: [];
    }

    public function getWithHint($id)
    {
        $sql = "/*FORCE_MASTER*/ SELECT * FROM {$this->table()} WHERE id = ?";

        return $this->db()->fetchAssoc($sql, [$id]) ?: [];
    }

    public function search($conditions, $orderbys, $start, $limit)
    {
        $builder = $this->_createQueryBuilder($conditions)
            ->select('*')
            ->setFirstResult($start)
            ->setMaxResults($limit);

        $declares = $this->declares();
        foreach ($orderbys ?: [] as $field => $direction) {
            if (!in_array($field, $declares['orderbys'], true)) {
                throw $this->createDaoException(sprintf("SQL order by field is only allowed '%s', but you give `{$field}`.", implode(',', $declares['orderbys'])));
            }
            if (!in_array(strtoupper($direction), ['ASC', 'DESC'], true)) {
                throw $this->createDaoException("SQL order by direction is only allowed `ASC`, `DESC`, but you give `{$direction}`.");
            }
            $builder->addOrderBy($field, $direction);
        }

        return $builder->execute()->fetchAll();
    }

    public function count($conditions)
    {
        $builder = $this->_createQueryBuilder($conditions)
            ->select('COUNT(*)');

        return $builder->execute()->fetchColumn(0);
    }

    public function table()
    {
        return $this->table;
    }

    /**
     * @return Connection
     */
    public function db()
    {
        return $this->biz['db'];
    }

    public function getByFields($fields)
    {
        $placeholders = array_map(function ($name) {
            return "{$name} = ?";
        }, array_keys($fields));

        $sql = "SELECT * FROM {$this->table()} WHERE ".implode(' AND ', $placeholders);

        return $this->db()->fetchAssoc($sql, array_values($fields)) ?: [];
    }

    public function findInField($field, $values)
    {
        if (empty($values)) {
            return [];
        }

        $marks = str_repeat('?,', count($values) - 1).'?';
        $sql = "SELECT * FROM {$this->table} WHERE {$field} IN ({$marks});";

        return $this->db()->fetchAll($sql, $values);
    }

    protected function _createQueryBuilder($conditions)
    {
        $conditions = array_filter($conditions, function ($value) {
            if ('' === $value || null === $value) {
                return false;
            }

            return true;
        });

        $builder = new DynamicQueryBuilder($this->db(), $conditions);
        $builder->from($this->table(), $this->table());

        $declares = $this->declares();
        $declares['conditions'] = isset($declares['conditions']) ? $declares['conditions'] : [];

        foreach ($declares['conditions'] as $condition) {
            $builder->andWhere($condition);
        }

        return $builder;
    }

    private function _getTimestampField($mode = null)
    {
        if (empty($this->timestamps)) {
            return null;
        }

        if ('created' === $mode) {
            return isset($this->timestamps[0]) ? $this->timestamps[0] : null;
        }
        if ('updated' === $mode) {
            return isset($this->timestamps[1]) ? $this->timestamps[1] : null;
        }

        throw $this->createDaoException('mode error.');
    }

    private function createDaoException($message = '', $code = 0)
    {
        return new DaoException($message, $code);
    }
}
